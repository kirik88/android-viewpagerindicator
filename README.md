# Overwiew
- - -

This is just a fork of [Android-ViewPagerIndicator by JakeWharton](https://github.com/JakeWharton/Android-ViewPagerIndicator) with:

1. Fixed issue with **NullPointerException** in **fakeDragBy** occurs in some indicators.
2. Updated **Android Support Library** to revision 19.1.0 (March 2014).